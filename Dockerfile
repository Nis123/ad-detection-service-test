FROM asia.gcr.io/surewaves-test/bizz-app-base

MAINTAINER shahrukh@surewaves.com

# installing supervisor and dependencies.

RUN apt-get update
RUN apt-get install gettext-base -y

# Install FFMPEG
RUN apt-get install -y software-properties-common
RUN add-apt-repository ppa:mc3man/trusty-media
RUN apt-get update
RUN apt-get install -y ffmpeg

# Installing dependencies

RUN apt-get update
RUN apt-get install rsyslog
RUN apt-get update
RUN apt-get install vim -y
RUN apt-get install cron -y
RUN apt-get install htop -y

# Setting time zone.

RUN sudo echo "Asia/Kolkata" > /etc/timezone
RUN sudo dpkg-reconfigure -f noninteractive tzdata

RUN sed -i 's/\;date\.timezone\ \=/date\.timezone\ \=\ Asia\/Kolkata/g' /etc/php5/cli/php.ini
RUN sed -i 's/\;date\.timezone\ \=/date\.timezone\ \=\ Asia\/Kolkata/g' /etc/php5/apache2/php.ini

# Setting files.

COPY . /app
WORKDIR /app
RUN composer update --no-scripts
RUN composer dump-autoload -o

# Setting config files for client.

ARG build_type

#RUN npm update -g npm
RUN npm install grunt@~0.4.5 --save-dev
RUN npm install -g grunt-cli
RUN grunt env:$build_type

# Setting cron tab file.
ADD ./crontab /app/var/cronfile
RUN crontab /app/var/cronfile

RUN chmod -R 777 /app/storage

COPY ./entrypoint.sh /

WORKDIR /
RUN chmod +x ./entrypoint.sh

CMD [ "/entrypoint.sh" ]