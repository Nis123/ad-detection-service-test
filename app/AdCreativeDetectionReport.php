<?php
/**
 * Created by PhpStorm.
 * User: sunil.pandab
 * Date: 12/26/2016
 * Time: 1:55 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdCreativeDetectionReport extends model
{
    public $timestamps = true;
    protected $table = 'sky_ad_creative_detection_report';

    public static function updateReportingToSkynetStatus($id)
    {
        self::where('id', $id)
            ->update(["report_to_skynet"=> "success"]);
    }
    
    public static function updateReportArchivalToSkynetStatus($id)
    {
        self::where('id', $id)
            ->update(["report_archival_to_skynet"=> "success"]);
    }
    
    public static function getDetectedAdsToReport( )
    {
        /*return self::join('sky_ad_creative_training_metadata', 'sky_ad_creative_training_metadata.acr_training_id', '=', 'sky_ad_creative_detection_report.acr_training_id')
            ->where('sky_ad_creative_detection_report.report_to_skynet', 'pending')
            ->select('sky_ad_creative_detection_report.id',
                'sky_ad_creative_detection_report.channel_id',
                'sky_ad_creative_training_metadata.sky_ad_creative_id as ad_creative_id',
                'sky_ad_creative_detection_report.detected_date as start_date',
                'sky_ad_creative_detection_report.detected_date as end_date',
                DB::raw('time(sky_ad_creative_detection_report.start_time) as start_time'),
                DB::raw('time(sky_ad_creative_detection_report.end_time) as end_time'))
            ->get()->toArray();*/
        
        return self::join('sky_ad_creative_training_metadata', 'sky_ad_creative_training_metadata.acr_training_id', '=', 'sky_ad_creative_detection_report.acr_training_id')
            ->where('sky_ad_creative_detection_report.report_to_skynet', 'pending')
            ->select('sky_ad_creative_detection_report.id',
                     'sky_ad_creative_detection_report.channel_id',
                     'sky_ad_creative_training_metadata.sky_ad_creative_id as ad_creative_id',
                     'sky_ad_creative_detection_report.start_time as start_datetime',
                     'sky_ad_creative_detection_report.end_time as end_datetime')
            ->get()->toArray();
    }
    
    public static function getArchivalsToReportToSkynet( )
    {
        return self::where('report_archival_to_skynet', 'pending')
            ->where('archival_path', '!=', '')
            ->select('id', 'archival_path')
            ->get()->toArray();
    }

    public static function insertReports($channelId, $trainedId, $detectedDate, $startTime, $endTime, $archivalId, $softDetect )
    {
        self::insert([ "channel_id"=> $channelId,
            "acr_training_id"=> $trainedId,
        "start_time"=> $startTime,
        "end_time"=> $endTime,
        "detected_date"=> $detectedDate,
        "archival_id"=> $archivalId,
        "soft_detect"=> $softDetect,]) ;
    }

    public static function updateArchival($archivalId, $archivalURL)
    {
        self::where('archival_id', $archivalId)
            ->update(["archival_path"=> $archivalURL]);
    }
}