<?php
/**
 * Created by PhpStorm.
 * User: sunil.pandab
 * Date: 12/20/2016
 * Time: 2:47 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class AdCreativeMetaData extends Model
{
    public $timestamps = true;
    protected $table = 'sky_ad_creative_training_metadata';

    public static function updateTrainingDetails( $input_values ){
        if(isset($input_values['data']) && count($input_values['data']) !=0)
        {
            foreach ($input_values['data'] as $key =>$val)
            {
                if($val['status'] == "success")
                {
                    self::where('sky_ad_creative_id', $val['content_id'])
                        ->update(["acr_training_id"=> $val['trained_id'],
                            "acr_training_status"=>$val['status']
                        ]);
                }
            }
            return true;
        }
        else
            return false;
    }

    public static function getContentsToTrain( ){
        
        return self::where('acr_training_status', 'pending' )
            ->select('sky_ad_creative_id as content_id',
                'content_url as content_path',
                'content_duration as content_duration',
                'content_md5 as content_md5')
            ->get()->toArray();
    }

    public static function updateContentsStatusToTrain($adCreativeId, $requestId, $status ){

        self::where('sky_ad_creative_id', $adCreativeId)
            ->update(["request_id"=> $requestId,
                      "acr_training_status"=> $status]);
    }

    public static function getContentCustomerAssociation($date)
    {
        return self::where('sky_ad_creative_training_metadata.acr_training_status', 'success' )
            ->where('sky_ad_creative_training_metadata.acr_sync_status', 'pending' )
            ->select('sky_ad_creative_training_metadata.acr_training_id as training_id')
            ->get()
            ->toArray() ;
    }

    public static function updateSyncStatus($traingId, $status)
    {
        self::where('acr_training_id', $traingId)
            ->update(["acr_sync_status"=> $status]);
    }
    
    public static function insertContentList( $core_service_contnet_id, $content_url, $content_duration, $content_md5 )
    {
        self::insert([ "sky_ad_creative_id"=> $core_service_contnet_id,
                                "content_url"=> $content_url,
                                "content_duration"=> $content_duration,
                                "content_md5"=> $content_md5]) ;
        return true;
    }
    
    public static function updateContentList( $core_service_contnet_id, $content_url, $content_duration, $content_md5 )
    {
        self::where('sky_ad_creative_id', $core_service_contnet_id)
            ->update(["content_url"=> $content_url,
                            "content_duration"=> $content_duration,
                            "content_md5"=> $content_md5,
                            "acr_training_status" => "pending",
                            "acr_sync_status" => "pending"]);
        return true;
    }
}