<?php
/**
 * Created by PhpStorm.
 * User: sunil.pandab
 * Date: 12/20/2016
 * Time: 6:19 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
class AdCreativeTrainingQueue extends Model
{
    public $timestamps = true;
    protected $table = 'sky_ad_creative_training_queue';

    public function InsertADCreativeTrainingRequest($adCreativeId, $requestId)
    {
        return self::insert([ "sky_ad_creative_id"=> $adCreativeId,
                              "acr_request_id"=> $requestId]) ;
    }
}