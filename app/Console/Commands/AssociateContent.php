<?php

namespace App\Console\Commands;

use App\Http\Controllers\Cron\CustomerChannelContentAssociation;
use Illuminate\Console\Command;

class AssociateContent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'content:associate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Content association';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $obj = new CustomerChannelContentAssociation();
        $obj->ChannelContentAssociation() ;
    }
}
