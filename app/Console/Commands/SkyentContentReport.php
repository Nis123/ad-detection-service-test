<?php

namespace App\Console\Commands;

use App\Http\Controllers\Reporting\ReportingController;
use Illuminate\Console\Command;

class SkynetContentReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'skynet:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reporting to skynet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $obj = new ReportingController();
        $obj->reportToSkynet() ;
    }
}
