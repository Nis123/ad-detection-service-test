<?php
/**
 * Created by PhpStorm.
 * User: Shahrukh
 * Date: 5/5/2016
 * Time: 11:18 AM
 */

namespace App;
use Illuminate\Support\Facades\DB ;
use Illuminate\Database\Eloquent\Model;

class CronLockHandler extends Model
{
    private $tableName = "tm_cron_lock" ;
    public function getFunctionLockStatusDateTime($md5)
    {
        $result = DB::table($this->tableName)
            ->select('status', 'lock_date_time')
            ->where('function_name_hash', "=",$md5)
            ->get();

        return json_decode(json_encode($result), true);

    }

    public function getLock($md5, $functionName)
    {
        $dateTime = date('Y-m-d H:i:s') ;

        DB::table($this->tableName)->insert(
            ['function_name' => $functionName, 'status' => 'locked','function_name_hash'=> $md5,'lock_date_time'=>$dateTime]
        );

    }

    public function updateLockStatusDateTime($md5, $dateTime)
    {
        DB::table($this->tableName)
            ->where('function_name_hash', $md5)
            ->update(['status'=>'locked', 'lock_date_time'=>$dateTime]);
    }

    public function releaseAcquiredLock($md5)
    {
        DB::table($this->tableName)
            ->where('function_name_hash', $md5)
            ->update(['status'=>'free']);
    }

}