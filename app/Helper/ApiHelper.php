<?php

/**
 * Created by PhpStorm.
 * User: sunil.pandab
 * Date: 12/26/2016
 * Time: 6:52 PM
 */
namespace App\Helper;
use Illuminate\Support\Facades\Config;
class ApiHelper
{
    public static function apiAuth($input, $apiName)
    {
        $SALTKEY = Config::get("constants.SALTKEY") ;
        $hash = crypt($input['timestamp'], $SALTKEY);
        $hash = crypt($input['appkey'], $hash);
        $hash = crypt($apiName, $hash);
        return $hash;
    }

    public static function checkInputs($postValues)
    {
        if(isset($postValues['timestamp']) && $postValues['timestamp'] !="")
        {
            if(isset($postValues['appkey']) && $postValues['appkey'] !="")
            {
                if(isset($postValues['authkey']) && $postValues['authkey'] !="")
                {
                    return [ "status"=>true, "message"=>"" ] ;
                }
                else
                {
                    $return = array("status"=>"error", "data"=>"missing parameter authkey!!!");
                    return [ "status"=>false, "message"=>json_encode($return) ] ;
                }
            }
            else
            {
                $return = array("status"=>"error", "data"=>"missing parameter appkey!!!");
                return [ "status"=>false, "message"=>json_encode($return) ] ;
            }
        }
        else
        {
            $return = array("status"=>"error", "data"=>"missing parameter timestamp!!!");
            return [ "status"=>false, "message"=>json_encode($return) ] ;

        }

    }
}