<?php

namespace App\Http\Controllers\Cron;
use App\Http\Controllers\Controller;
use App\Lib\CronLockManager;
use Exception;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Config;
use  App\AdCreativeMetaData;
use App\Helper\ApiHelper;
use App\Lib\AppLogger;

/**
 * Created by PhpStorm.
 * User: sunil.pandab
 * Date: 12/21/2016
 * Time: 4:24 PM
 */
class CustomerChannelContentAssociation extends Controller
{
    public function __construct()
    {
    }

    public function ChannelContentAssociation($addDay=0)
    {
        $log = new AppLogger("ChannelContentAssociation") ;
        $functionName = "ChannelContentAssociation" ;
        $cronLockObject = new CronLockManager() ;
        $FINGER_PRINT_APPKEY = Config::get("constants.FINGER_PRINT_APPKEY");
        $ACR_SERVER = Config::get("constants.ACR_SERVER");

        if ($cronLockObject->lock($functionName) != false)
        {
            $date = date( "Y-m-d", strtotime("+$addDay day") ) ;
            $log->addLogInfo("Cron for ChannelContentAssociation runs for", ["date"=>$date]) ;
            try {
                $res = AdCreativeMetaData::getContentCustomerAssociation($date);
                $log->addLogInfo("data to be sending for ChannelContentAssociation", ["data"=>$res]) ;
                if (count($res) > 0) {

                    $dataArray = array();
                    $customer_id = "1";
                    $suresync_enabled = "";
                    foreach ($res as $value) {

                        $trainedId = $value['training_id'];
                        if (!empty($dataArray["_id" . $customer_id]["trained_id"])) {
                            $trainedId .= "," . $dataArray["_id" . $customer_id]["trained_id"];
                        }
                        $dataArray["_id" . $customer_id] =
                            array(
                                "customer_id" => $customer_id,
                                "trained_id" => $trainedId,
                                "suresync_enabled" => $suresync_enabled
                            );
                    }

                    $input['timestamp'] = date('Y-m-d H:i:s');
                    $input['appkey'] = $FINGER_PRINT_APPKEY;
                    $authkey = ApiHelper::apiAuth($input, "content/associate");

                    $compressed = gzcompress(json_encode(array($dataArray["_id" . $customer_id])), 9);
                    $encode = base64_encode($compressed);

                    $data = json_encode(
                        array(
                            "authkey" => $authkey,
                            "timestamp" => $input['timestamp'],
                            "appkey" => $FINGER_PRINT_APPKEY,
                            "date" => $date,
                            "data" => $encode)
                    );

                    $url = $ACR_SERVER . "content/associate/";
                    $result = $this->postData($url, $data);
                    $result = json_decode($result['body'], true);
                    if ($result['status'] == "success") {
                        foreach ($res as $key => $val) {
                            AdCreativeMetaData::updateSyncStatus($val['training_id'], "success");
                        }
                    }else{
                    }
                }
            }catch (Exception $e){
                $log->addLogInfo("error", ["exception details"=>$e]) ;
            }finally{
                $cronLockObject->unlock($functionName) ;
            }
        }
    }

    private function postData($url, $data)
    {
        try{
            $client = new \GuzzleHttp\Client() ;
            $responseObj = $client->request('POST',
                $url,
                ["form_params"=>array('request' => $data)]
            ) ;

            return array( "body" => $responseObj->getBody()->getContents(),
                "status_code"=> $responseObj->getStatusCode()
            ) ;
        }catch(RequestException $e){
            return array( "status_code"=>503 ) ;
        }
    }
}