<?php
namespace App\Http\Controllers\Cron;
use App\Http\Controllers\Controller;
use App\Lib\CronLockManager;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Config;
use  App\AdCreativeMetaData;
use App\Helper\ApiHelper;
use App\Lib\AppLogger;

/**
 * Created by PhpStorm.
 * User: sunil.pandab
 * Date: 12/20/2016
 * Time: 4:29 PM
 */

class SendContentToTrainController extends Controller
{
    public function __construct()
    {
    }

    public function trainContent()
    {
        $functionName = "trainContent" ;
        $cronLockObject = new CronLockManager() ;
        $log = new AppLogger("trainContent") ;
        if ($cronLockObject->lock($functionName) != false) {

            $FINGER_PRINT_APPKEY = Config::get("constants.FINGER_PRINT_APPKEY");
            $ACR_SERVER = Config::get("constants.ACR_SERVER");

            $res = AdCreativeMetaData::getContentsToTrain();
            $log->addLogInfo("data sent to train", ["request"=>$res]) ;
            if (is_array($res) && count($res) != 0) {
                $input['timestamp'] = date('Y-m-d H:i:s');
                $input['appkey'] = $FINGER_PRINT_APPKEY;
                $authkey = ApiHelper::apiAuth($input, "content/ingest");
                $data = json_encode(array("authkey" => $authkey, "timestamp" => $input['timestamp'], "appkey" => $FINGER_PRINT_APPKEY, "data" => $res));

                $url = $ACR_SERVER . "content/ingest/";
                $result = $this->postData($url, $data);
                $result = json_decode($result['body'], true) ;
                $log->addLogInfo("response", ["response"=>$result]) ;
                if ($result['status'] == "success") {
                    $requestId = $result['request_id'];
                    foreach ($res as $key => $val) {
                        AdCreativeMetaData::updateContentsStatusToTrain($val['content_id'], $requestId, "processing");
                    }
                }
            }else{}

            $cronLockObject->unlock($functionName) ;
        }

    }

    private function postData($url, $data)
    {
        try{
            $client = new \GuzzleHttp\Client() ;
            $responseObj = $client->request('POST',
                $url,
                ["form_params"=>array('request' => $data)]
            ) ;

            return array( "body" => $responseObj->getBody()->getContents(),
                "status_code"=> $responseObj->getStatusCode()
            ) ;
        }catch(RequestException $e){
            return array( "status_code"=>503 ) ;
        }
    }
}