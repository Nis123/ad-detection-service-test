<?php
namespace App\Http\Controllers\Reporting;
use App\Http\Controllers\Controller;
use App\AdCreativeDetectionReport;
use App\Lib\AppLogger;
//use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use App\Helper\ApiHelper;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use App\Lib\CronLockManager;
/**
 * Created by PhpStorm.
 * User: sunil.pandab
 * Date: 12/26/2016
 * Time: 1:50 PM
 */
class ReportingController extends Controller
{
    public function __construct()
    {

    }
    
    private function removeIdFromGivenResultSet(&$finalRes) {
        foreach($finalRes as $key => $value) {
            
            foreach($value as $assI =>$actualValue){
                
                if($assI === "id") {
                    unset($finalRes[$key][$assI]) ;
                }
            }
        }
        
        return $finalRes ;
    }
    
    public function reportToSkynet()
    {
        $functionName = "reportToSkynet" ;
        $cronLockObject = new CronLockManager() ;
        $log = new AppLogger("reportToSkynet") ;
        if ($cronLockObject->lock($functionName) != false)
        {
            try {
                $res = AdCreativeDetectionReport::getDetectedAdsToReport();
                if(count($res) > 0)
                {
                    $finalRes = $res ;
                    $finalResult = $res; //$this->removeIdFromGivenResultSet($finalRes) ;

                    foreach ($finalResult as $key => $value)
                    {
                        $finalResult[$key]["is_completed"]= true;
                    }

                    $log->addLogInfo("request of Report to skynet", ["reportRequest"=>$finalResult]) ;
                    $url = Config::get("constants.SKYNET_REPORT_SERVER") . "v1/br/reports/many/";
                    $response = $this->postData($url, $finalResult);
                    $log->addLogInfo("response of Report to skynet", ["reportResponse"=>$response]) ;

                    //update report sent status
                    foreach ($res as $key => $value)
                    {
                        AdCreativeDetectionReport::updateReportingToSkynetStatus($res[$key]["id"]); 
                    }
                }
                else
                {
                    $log->addLogInfo("request of Report to skynet", ["reportRequest"=>"There is no report to send"]) ;
                }
            }catch (Exception $e){
                $log->addLogInfo("error", ["exception details"=>$e]) ;
            }finally{
                $cronLockObject->unlock($functionName) ;
            }
        }
    }
    
    public function reportArchivalToSkynet()
    {
        $functionName = "reportArchivalToSkynet" ;
        $cronLockObject = new CronLockManager() ;
        $log = new AppLogger("reportArchivalToSkynet") ;
        if ($cronLockObject->lock($functionName) != false)
        {
            try {
                $res = AdCreativeDetectionReport::getArchivalsToReportToSkynet();
                //print_r($res);exit;
                if(count($res) > 0)
                {
                    $log->addLogInfo("request of Archival Report to skynet", ["reportArchivalRequest"=>$res]) ;
                    $url = Config::get("constants.SKYNET_REPORT_SERVER") . "v1/br/reports/archival/";
                    $response = $this->postData($url, $res);
                    $log->addLogInfo("response of Archival Report to skynet", ["archivalReportResponse"=>$response]) ;

                    //update archival sent status
                    foreach ($res as $key => $value)
                    {
                        AdCreativeDetectionReport::updateReportArchivalToSkynetStatus($res[$key]["id"]); 
                    }
                }
                else
                {
                    $log->addLogInfo("request of Archival Report to skynet", ["reportArchivalRequest"=>"There is no archival to send"]) ;
                }
            }catch (Exception $e){
            $log->addLogInfo("error", ["exception details"=>$e]) ;
            }finally{
                $cronLockObject->unlock($functionName) ;
            }
        }
    }

    public function detectedAdReports()
    {
        $log = new AppLogger("detectedAdReports") ;

        $input_values = json_decode(Input::get("request"), true);

        $log->addLogInfo("Report from DX", ["reportData"=>$input_values]) ;

        $auth = ApiHelper::apiAuth($input_values, "AdvertisementsLog");
        if ($input_values['authkey'] == $input_values['authkey'])
        {
            $archivalId = $input_values['archival_id'];
            $channelId = $input_values['channel_id'];
            foreach($input_values['detects'] as $key => $val)
            {
                AdCreativeDetectionReport::insertReports($channelId, $val["trained_id"], $val["detected_date"], $val["start_time"], $val["end_time"], $archivalId, $val["soft_detect"]);
            }
            $return = array("status" => "success", "data" => "Report added successful");
        }
        else{
            $return = array("status" => "error", "data" => "Authentication Failed");
        }

        $log->addLogInfo("Response sending to DX", ["response"=>$return]) ;
        return Response::json($return, 200) ;

    }

    public function updateArchivalPath()
    {
        $log = new AppLogger("updateArchivalPath") ;
        $input_values = json_decode(Input::get("request"), true);
        $log->addLogInfo("Archival Path from DX", ["archivalData"=>$input_values]) ;
        $auth = ApiHelper::apiAuth($input_values, "AdvertisementsLog");
        if ($input_values['authkey'] == $input_values['authkey'])
        {
            foreach($input_values['data'] as $key => $val)
            {
                AdCreativeDetectionReport::updateArchival($val["archival_id"], $val["archival_url"]);
            }
            $return = array("status" => "success", "data" => "Archival Path updated successful");
        }
        else{
            $return = array("status" => "error", "data" => "Authentication Failed");
        }

        $log->addLogInfo("Response sending to DX", ["response"=>$return]) ;
        return Response::json($return, 200) ;
        
    }

    private function postData($url, $data)
    {
        try{
            $client = new \GuzzleHttp\Client() ;
            $responseObj = $client->request('POST',
                $url,
                ['json' => $data]
            ) ;

            return array( "body" => json_decode($responseObj->getBody())
            ) ;
        }catch(RequestException $e){
            return array( "status_code"=>503 ) ;
        }
    }
}
