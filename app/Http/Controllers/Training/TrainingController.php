<?php
/**
 * Created by PhpStorm.
 * User: sunil.pandab
 * Date: 12/16/2016
 * Time: 7:45 PM
 */
namespace App\Http\Controllers\Training;
use Illuminate\Http\Request;
use App\AdCreativeMetaData;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Helper\ApiHelper;
use App\Lib\AppLogger;

class TrainingController extends Controller
{
    public function __construct()
    {}

    public function updateTrainingDetails(){
        $log = new AppLogger("UpdateTrainingDetails") ;
        $input_values = json_decode(Input::get("request"), true);
        $log->addLogInfo("Request JSON: ", ["data"=>$input_values]) ;
        $status = ApiHelper::checkInputs($input_values) ;
        if ( $status["status"] ) {
            $auth = ApiHelper::apiAuth($input_values, "UpdateContentFingerPrint");
            if ($input_values['authkey'] == $input_values['authkey']) {
                $res = AdCreativeMetaData::updateTrainingDetails($input_values);
                if ($res) {
                    return  array("status" => "success", "data" => $res);
                } else
                    return array("status" => "error", "data" => "Content doesn't exist");
            } else
                return array("status" => "error", "data" => "Authentication Failed !!!");

        }else{
            return array("status" => "error", "data" =>$status["message"]) ;
        }
    }
    
    public function contentListToDetect(){
        $log = new AppLogger("Contents-To-Detect") ;
        $input_values = Input::get();
        $status = ApiHelper::checkInputs($input_values) ;
        if ( $status ) {
            $auth = ApiHelper::apiAuth($input_values, "UpdateContentFingerPrint");
            if ($input_values['authkey'] == $input_values['authkey']) { 
                $log->addLogInfo("Request JSON: ", ["data"=>$input_values['data']]) ;
                
                if(isset($input_values['data']) && count($input_values['data']) !=0)
                {
                    foreach ($input_values['data'] as $key =>$val)
                    {
                        if($val["is_changed"] == 1) //update the details into DB
                        {
                            $resUpdate = AdCreativeMetaData::updateContentList($val["core_service_content_id"], $val["content_url"], $val["content_duration"], $val["content_md5"]);
                        }
                        else //insert into DB
                        {
                            $resInsert = AdCreativeMetaData::insertContentList($val["core_service_content_id"], $val["content_url"], $val["content_duration"], $val["content_md5"]);
                        }
                    }
                    $log->addLogInfo("success", ["data"=>"success"]) ;
                    return  array("status" => "success", "data" => "success");
                }
                else
                {
                    $log->addLogInfo("Error", ["data"=>"No data found to insert or update"]) ;
                    return array("status" => "error", "data" => "No Data Found");
                }
            } else
            {
                $log->addLogInfo("Error", ["data"=>"Authentication Failed !!!"]) ;
                return array("status" => "error", "data" => "Authentication Failed !!!");
            }

        }else{
            $log->addLogInfo("Error", ["data"=>$status["message"]]) ;
            return array("status" => "error", "data" =>$status["message"]) ;
        }
    }
}