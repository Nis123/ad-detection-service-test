<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

/*Route::get('train', 'Training\TrainingController@train') ;*/
Route::post('training', 'Training\TrainingController@updateTrainingDetails') ;
Route::get('report-to-skynet', 'Reporting\ReportingController@reportToSkynet') ;
Route::get('report-archival-to-skynet', 'Reporting\ReportingController@reportArchivalToSkynet') ;
Route::post('detected-ad-reports', 'Reporting\ReportingController@detectedAdReports') ;
Route::post('update-archival', 'Reporting\ReportingController@updateArchivalPath') ;
Route::post('contents-to-detect', 'Training\TrainingController@contentListToDetect') ;

/*testing*/
Route::get('content-training', 'Cron\SendContentToTrainController@trainContent') ;
Route::get('content-channel-association/{addDay}', 'Cron\CustomerChannelContentAssociation@ChannelContentAssociation') ;

