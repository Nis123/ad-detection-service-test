<?php
/**
 * Created by PhpStorm.
 * User: sunil.pandab
 * Date: 1/4/2017
 * Time: 3:23 PM
 */

namespace App\Lib;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;


class AppLogger
{
    private $log ;
    public function __construct($loggerName){

        $fileName = $loggerName.date('Y-m-d').'.log' ;
        $this->log = new Logger($loggerName);
        $this->log->pushHandler(new StreamHandler(storage_path('logs/'.$fileName), Logger::DEBUG));
        $this->log->addInfo('New Instance=========================================================>');
    }

    public function addLogInfo($note, $arrayMessage){

        $this->log->info($note, $arrayMessage) ;

    }
    public function addLogError($note, $arrayMessage){

        $this->log->error($note, $arrayMessage) ;

    }
    public function addLogCritical($note, $arrayMessage){

        $this->log->critical($note, $arrayMessage) ;

    }
}
