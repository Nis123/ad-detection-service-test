<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28/6/16
 * Time: 5:36 PM
 */

namespace App\Lib ;
use App\CronLockHandler ;

class CronLockManager
{

    private $cronLockHandlerObj;

    function __construct() {

        $this->cronLockHandlerObj = new CronLockHandler() ;
    }

    public function lock( $functionName ) {

        $returnVale = false ;

        $resultStatusLockDateTime =  $this->cronLockHandlerObj->getFunctionLockStatusDateTime( md5( $functionName ) ) ;

        if( !empty($resultStatusLockDateTime) ){

            $status         = $resultStatusLockDateTime[0]["status"] ;
            $lockedDateTime = $resultStatusLockDateTime[0]["lock_date_time"] ;

            if( $this->checkStatus($status) && $this->checkLockDuration($lockedDateTime) ){

                $this->reAcquireLockStatusDateTime( $functionName ) ;
                $returnVale = true ;

            }else if( !$this->checkStatus($status) ){

                $this->reAcquireLockStatusDateTime( $functionName ) ;
                $returnVale = true ;

            }else if( $this->checkStatus($status) ){

                $returnVale = false ;

            }

        }else{

            $this->acquireLock( $functionName ) ;
            $returnVale = true ;
        }

        return $returnVale ;
    }

    private function acquireLock( $functionName ) {

        $this->cronLockHandlerObj->getLock( md5( $functionName ), $functionName ) ;

    }

    private function checkStatus( $status ){

        $returnVale = false ;

        if( trim($status) == "locked" ){

            $returnVale = true ;

        }

        return $returnVale ;
    }

    private function checkLockDuration( $lockDateTime ){

        $returnVale = false ;

        $time = time();

        if( $time - strtotime($lockDateTime) > (4*3600) ){

            $returnVale = true ;

        }

        return $returnVale ;
    }

    private function reAcquireLockStatusDateTime( $functionName ){

        $dateTime = date('Y-m-d H:i:s') ;

        $this->cronLockHandlerObj->updateLockStatusDateTime( md5($functionName), $dateTime ) ;

    }

    public function unlock( $functionName ) {

        $this->cronLockHandlerObj->releaseAcquiredLock( md5($functionName) ) ;

    }

}