<?php
/**
 * Created by PhpStorm.
 * User: sunil.pandab
 * Date: 12/20/2016
 * Time: 4:53 PM
 */

return [ "FINGER_PRINT_APPKEY" => "5548db09faacb624503064f9",
         "SALTKEY"=> "$2a$07$2NAKIAIRC3SCXSWPGUBS6A$",
         "ACR_SERVER"=> "http://".env('ACR_IP_PORT')."/",
         "SKYNET_REPORT_SERVER"=> "http://".env('SKYNET_IP_PORT')."/"] ;

