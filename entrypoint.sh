#!/usr/bin/env bash
cp app/env/$APP_ENV.env app/.env
/usr/sbin/cron
rm -rf /run/httpd/* /tmp/httpd*
rm /var/run/apache2/apache2.pid

./run.sh
